package ru.korkmasov.tsc.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}
