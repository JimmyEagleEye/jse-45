package ru.korkmasov.tsc.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.exception.AbstractException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws AbstractException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    static void incorrectValue() {
        System.out.println("Incorrect values");
    }

}
