package ru.korkmasov.tsc.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";
    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "5";
    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";
    @NotNull
    private static final int BACKUP_INTERVAL_MIN = 1;
    @NotNull
    private static final String FILE_NAME = "application.properties";
    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    private static final String SCANNER_INTERVAL_DEFAULT = "5";
    @NotNull
    private static final String SCANNER_INTERVAL_KEY = "scanner.interval";
    private static final int SCANNER_INTERVAL_MIN = 1;
    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";
    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";
    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";
    @NotNull
    String JDBC_USER_KEY = "jdbc.user";
    @NotNull
    String JDBC_PASSWORD_KEY = "jdbc.password";
    @NotNull
    String JDBC_URL_KEY = "jdbc.url";
    @NotNull
    String JDBC_DRIVER_KEY = "jdbc.driver";
    @NotNull
    String HIBERNATE_DIALECT_KEY = "hibernate.dialect";
    @NotNull
    String HIBERNATE_HBM2DDL_AUTO_KEY = "hibernate.hbm2ddl_auto";
    @NotNull
    String HIBERNATE_SHOW_SQL_KEY = "hibernate.show_sql";
    @NotNull
    private final Properties properties = new Properties();


    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_VERSION_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_VERSION_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public int getBackupInterval() {
        @Nullable final String systemProperty = System.getProperty(BACKUP_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), 5000);
        @Nullable final String environmentProperty = System.getenv(BACKUP_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), 5000);
        final int interval = Integer.parseInt(properties.getProperty(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT));
        return Math.max(interval, 5000);
    }

    @Override
    public int getScannerInterval() {
        @Nullable final String systemProperty = System.getProperty(SCANNER_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), SCANNER_INTERVAL_MIN);
        @Nullable final String environmentProperty = System.getenv(SCANNER_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), SCANNER_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(SCANNER_INTERVAL_KEY, SCANNER_INTERVAL_DEFAULT));
        return Math.max(interval, SCANNER_INTERVAL_MIN);
    }

    @Override
    public @NotNull String getServerHost() {
        @Nullable final String systemProperty = System.getProperty(SERVER_HOST_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(SERVER_HOST_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public int getServerPort() {
        @Nullable final String systemProperty = System.getProperty(SERVER_PORT_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(SERVER_PORT_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT));
    }

    @Override
    public int getSessionCycle() {
        @Nullable final String systemProperty = System.getProperty(SESSION_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(SESSION_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT)
        );
    }

    @Override
    public @NotNull String getSessionSalt() {
        @Nullable final String systemProperty = System.getProperty(SESSION_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(SESSION_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @Nullable
    public String getJdbcUser() {
        if (System.getenv().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_USER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_USER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_USER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcPassword() {
        if (System.getenv().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getenv(JDBC_PASSWORD_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_PASSWORD_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_PASSWORD_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcUrl() {
        if (System.getenv().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getenv(JDBC_URL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_URL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_URL_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcDriver() {
        if (System.getenv().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_DRIVER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_DRIVER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_DRIVER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateDialect() {
        if (System.getenv().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_DIALECT_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_DIALECT_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_DIALECT_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateBM2DDLAuto() {
        if (System.getenv().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateShowSql() {
        if (System.getenv().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_SHOW_SQL_KEY);
        return value;
    }

}
