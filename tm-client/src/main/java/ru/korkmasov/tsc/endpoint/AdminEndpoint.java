package ru.korkmasov.tsc.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tsc.korkmasov.ru/", name = "AdminEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/closeAllByUserIdRequest", output = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/closeAllByUserIdResponse")
    @RequestWrapper(localName = "closeAllByUserId", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.CloseAllByUserId")
    @ResponseWrapper(localName = "closeAllByUserIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.CloseAllByUserIdResponse")
    public void closeAllByUserId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/removeByLoginRequest", output = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/removeByLoginResponse")
    @RequestWrapper(localName = "removeByLogin", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveByLogin")
    @ResponseWrapper(localName = "removeByLoginResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveByLoginResponse")
    public void removeByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/lockByLoginRequest", output = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/lockByLoginResponse")
    @RequestWrapper(localName = "lockByLogin", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LockByLogin")
    @ResponseWrapper(localName = "lockByLoginResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LockByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.UserEndpoint lockByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/findAllByUserIdRequest", output = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/findAllByUserIdResponse")
    @RequestWrapper(localName = "findAllByUserId", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindAllByUserId")
    @ResponseWrapper(localName = "findAllByUserIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindAllByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.korkmasov.tsc.endpoint.Session> findAllByUserId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/unlockByLoginRequest", output = "http://endpoint.tsc.korkmasov.ru/AdminEndpoint/unlockByLoginResponse")
    @RequestWrapper(localName = "unlockByLogin", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UnlockByLogin")
    @ResponseWrapper(localName = "unlockByLoginResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UnlockByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.UserEndpoint unlockByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );
}
