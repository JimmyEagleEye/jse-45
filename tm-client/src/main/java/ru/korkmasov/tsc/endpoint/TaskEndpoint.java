package ru.korkmasov.tsc.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tsc.korkmasov.ru/", name = "TaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByNameResponse")
    @RequestWrapper(localName = "finishTaskByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskByName")
    @ResponseWrapper(localName = "finishTaskByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task finishTaskByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskResponse")
    @RequestWrapper(localName = "addTask", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTask")
    @ResponseWrapper(localName = "addTaskResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task addTask(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "task", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Task task
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/unbindTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/unbindTaskByIdResponse")
    @RequestWrapper(localName = "unbindTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UnbindTaskById")
    @ResponseWrapper(localName = "unbindTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UnbindTaskByIdResponse")
    public void unbindTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "taskId", targetNamespace = "")
                    java.lang.String taskId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByIdResponse")
    @RequestWrapper(localName = "findTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskById")
    @ResponseWrapper(localName = "findTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task findTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/bindTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/bindTaskByIdResponse")
    @RequestWrapper(localName = "bindTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.BindTaskById")
    @ResponseWrapper(localName = "bindTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.BindTaskByIdResponse")
    public void bindTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "taskId", targetNamespace = "")
                    java.lang.String taskId,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/clearTaskRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/clearTaskResponse")
    @RequestWrapper(localName = "clearTask", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.ClearTask")
    @ResponseWrapper(localName = "clearTaskResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.ClearTaskResponse")
    public void clearTask(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByIndexResponse")
    @RequestWrapper(localName = "startTaskByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskByIndex")
    @ResponseWrapper(localName = "startTaskByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task startTaskByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByIdResponse")
    @RequestWrapper(localName = "removeTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskById")
    @ResponseWrapper(localName = "removeTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskByIdResponse")
    public void removeTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskAllRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskAllResponse")
    @RequestWrapper(localName = "addTaskAll", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTaskAll")
    @ResponseWrapper(localName = "addTaskAllResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTaskAllResponse")
    public void addTaskAll(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "collection", targetNamespace = "")
                    java.util.List<ru.korkmasov.tsc.endpoint.Task> collection
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByProjectIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByProjectIdResponse")
    @RequestWrapper(localName = "findTaskByProjectId", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByProjectId")
    @ResponseWrapper(localName = "findTaskByProjectIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.korkmasov.tsc.endpoint.Task> findTaskByProjectId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/updateTaskByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/updateTaskByIndexResponse")
    @RequestWrapper(localName = "updateTaskByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateTaskByIndex")
    @ResponseWrapper(localName = "updateTaskByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task updateTaskByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/updateTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/updateTaskByIdResponse")
    @RequestWrapper(localName = "updateTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateTaskById")
    @ResponseWrapper(localName = "updateTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task updateTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByIdResponse")
    @RequestWrapper(localName = "finishTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskById")
    @ResponseWrapper(localName = "finishTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task finishTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByIndexResponse")
    @RequestWrapper(localName = "findTaskByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByIndex")
    @ResponseWrapper(localName = "findTaskByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task findTaskByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByIdResponse")
    @RequestWrapper(localName = "startTaskById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskById")
    @ResponseWrapper(localName = "startTaskByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task startTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/startTaskByNameResponse")
    @RequestWrapper(localName = "startTaskByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskByName")
    @ResponseWrapper(localName = "startTaskByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task startTaskByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskByNameResponse")
    @RequestWrapper(localName = "findTaskByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByName")
    @ResponseWrapper(localName = "findTaskByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task findTaskByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByIndexResponse")
    @RequestWrapper(localName = "removeTaskByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskByIndex")
    @ResponseWrapper(localName = "removeTaskByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskByIndexResponse")
    public void removeTaskByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskWithNameRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/addTaskWithNameResponse")
    @RequestWrapper(localName = "addTaskWithName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTaskWithName")
    @ResponseWrapper(localName = "addTaskWithNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddTaskWithNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task addTaskWithName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskResponse")
    @RequestWrapper(localName = "removeTask", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTask")
    @ResponseWrapper(localName = "removeTaskResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskResponse")
    public void removeTask(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "task", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Task task
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/removeTaskByNameResponse")
    @RequestWrapper(localName = "removeTaskByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskByName")
    @ResponseWrapper(localName = "removeTaskByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveTaskByNameResponse")
    public void removeTaskByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/finishTaskByIndexResponse")
    @RequestWrapper(localName = "finishTaskByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskByIndex")
    @ResponseWrapper(localName = "finishTaskByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Task finishTaskByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskAllRequest", output = "http://endpoint.tsc.korkmasov.ru/TaskEndpoint/findTaskAllResponse")
    @RequestWrapper(localName = "findTaskAll", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskAll")
    @ResponseWrapper(localName = "findTaskAllResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindTaskAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.korkmasov.tsc.endpoint.Task> findTaskAll(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );
}
