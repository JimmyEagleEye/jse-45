package ru.korkmasov.tsc.command.project;


import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.endpoint.ProjectEndpoint;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectByIdStartCommand extends AbstractProjectCommand {
    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-start-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Start project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().startProjectById(serviceLocator.getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
    }

}

