package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static ru.korkmasov.tsc.util.ValidationUtil.isEmpty;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-create";
    }

    @Override
    public @NotNull String description() {
        return "Create project";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        //@NotNull final Project project = add(name, description);
        //serviceLocator.getProjectEndpoint().addProject(serviceLocator.getSession(), project);
    }

}
