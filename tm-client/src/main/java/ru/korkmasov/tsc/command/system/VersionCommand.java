package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;
import com.jcabi.manifests.Manifests;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
        System.out.println("[BUILD]");
        System.out.println(Manifests.read("build"));
    }

}
