package ru.korkmasov.tsc.command.project;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.enumerated.Role;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("STARTED: " + project.getDateStart());
        System.out.println("FINISHED: " + project.getDateFinish());
    }

    @Nullable
    protected String toString(ru.korkmasov.tsc.endpoint.@NotNull Project project) {
        return project.getId() + ": " + project.getName();
    }

}
