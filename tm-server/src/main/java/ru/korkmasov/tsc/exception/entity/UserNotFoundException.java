package ru.korkmasov.tsc.exception.entity;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Error! UserDTO not found...");
    }

}
