package ru.korkmasov.tsc.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.dto.IProjectDtoRepository;
import ru.korkmasov.tsc.api.repository.dto.ITaskDtoRepository;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.api.service.dto.IProjectTaskDtoService;
import ru.korkmasov.tsc.dto.TaskDto;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.repository.dto.ProjectDtoRepository;
import ru.korkmasov.tsc.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDto> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.bindTaskToProjectById(userId, taskId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.unbindTaskById(userId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}