package ru.korkmasov.tsc.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.dto.ISessionDtoRepository;
import ru.korkmasov.tsc.dto.SessionDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDtoRepository extends AbstractDtoRepository<SessionDto> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<SessionDto> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM SessionDto e WHERE e.userId = :userId", SessionDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<SessionDto> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", SessionDto.class).getResultList();
    }

    public SessionDto findById(@Nullable final String id) {
        return entityManager.find(SessionDto.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionDto reference = entityManager.getReference(SessionDto.class, id);
        entityManager.remove(reference);
    }
}