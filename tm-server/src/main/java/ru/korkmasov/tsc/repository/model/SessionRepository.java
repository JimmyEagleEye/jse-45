package ru.korkmasov.tsc.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.model.ISessionRepository;
import ru.korkmasov.tsc.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public Session getReference(@NotNull final String id) {
        return entityManager.getReference(Session.class, id);
    }

    @Override
    public List<Session> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        Session reference = entityManager.getReference(Session.class, id);
        entityManager.remove(reference);
    }
}