package ru.korkmasov.tsc.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.dto.IProjectDtoRepository;
import ru.korkmasov.tsc.dto.ProjectDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDtoRepository extends AbstractDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public ProjectDto findById(@Nullable final String id) {
        return entityManager.find(ProjectDto.class, id);
    }

    public void remove(final ProjectDto entity) {
        ProjectDto reference = entityManager.getReference(ProjectDto.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        ProjectDto reference = entityManager.getReference(ProjectDto.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<ProjectDto> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDto e", ProjectDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<ProjectDto> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.userId = :userId", ProjectDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ProjectDto findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectDto e WHERE e.id = :id AND e.userId = :userId", ProjectDto.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public ProjectDto findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectDto e WHERE e.name = :name AND e.userId = :userId", ProjectDto.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public ProjectDto findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectDto e WHERE e.userId = :userId", ProjectDto.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("userId", userId)
                        .setFirstResult(index)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDto e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }


}