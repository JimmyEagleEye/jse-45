package ru.korkmasov.tsc.api;


import ru.korkmasov.tsc.dto.AbstractDtoEntity;

public interface IServiceDto<E extends AbstractDtoEntity> extends IRepositoryDto<E> {

}