package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.UserDto;
import ru.korkmasov.tsc.enumerated.Role;

public interface IAuthService {

    String getUserId();

    UserDto getUser();

    boolean isAuth();

    void checkRoles(Role... roles);

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
